import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/dashboard',
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('../modules/dashboard/dashboard-routing.module').then((m) => m.DashboardRoutingModule),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('../modules/authentication/authentication-routing.module').then((m) =>
        m.AuthenticationRoutingModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
