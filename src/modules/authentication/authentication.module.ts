import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import * as AuthContainers from './containers';
import * as AuthComponents from './components';


@NgModule({
  declarations: [
    ...AuthComponents.components,
    ...AuthContainers.containers
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ...AuthComponents.components,
    ...AuthContainers.containers
  ]
})
export class AuthenticationModule { }
