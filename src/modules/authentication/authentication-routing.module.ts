import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import * as AuthContainers from "./containers";
import {AuthenticationModule} from "./authentication.module";

const routes: Routes = [
  {
    path: '',
    canActivate: [],
    component: AuthContainers.LoginBaseComponent,
  }
];


@NgModule({
  imports: [AuthenticationModule,RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
