import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  loggedIn: boolean = false;
  userEmail: string = '';

  constructor(private http: HttpClient) {
  }

  login() {
    window.open('http://localhost:3000/auth/google', '_self');
  }

  logout() {
    this.loggedIn = false;
    this.userEmail = '';
    localStorage.removeItem('token');
  }

}
