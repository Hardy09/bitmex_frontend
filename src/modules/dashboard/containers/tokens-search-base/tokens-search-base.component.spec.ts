import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TokensSearchBaseComponent } from './tokens-search-base.component';

describe('TokensSearchBaseComponent', () => {
  let component: TokensSearchBaseComponent;
  let fixture: ComponentFixture<TokensSearchBaseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TokensSearchBaseComponent]
    });
    fixture = TestBed.createComponent(TokensSearchBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
