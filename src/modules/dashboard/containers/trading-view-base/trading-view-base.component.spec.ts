import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingViewBaseComponent } from './trading-view-base.component';

describe('TradingViewBaseComponent', () => {
  let component: TradingViewBaseComponent;
  let fixture: ComponentFixture<TradingViewBaseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TradingViewBaseComponent]
    });
    fixture = TestBed.createComponent(TradingViewBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
