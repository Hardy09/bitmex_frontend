import {TokensSearchBaseComponent} from "./tokens-search-base/tokens-search-base.component";
import {TradingViewBaseComponent} from "./trading-view-base/trading-view-base.component";
import {DashboardBaseComponent} from "./dashboard-base/dashboard-base.component";

export const containers = [
  TokensSearchBaseComponent,
  TradingViewBaseComponent,
  DashboardBaseComponent
];

export * from './dashboard-base/dashboard-base.component';
export * from './trading-view-base/trading-view-base.component';
export * from './tokens-search-base/tokens-search-base.component';
