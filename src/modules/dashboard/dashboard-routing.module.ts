import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import * as DashboardContainers from './containers';
import {DashboardModule} from "./dashboard.module";

const routes: Routes = [
  {
    path: '',
    canActivate: [],
    component: DashboardContainers.DashboardBaseComponent,
  }
];

@NgModule({
  imports: [DashboardModule,RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
