import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from '@angular/core';
import {createChart, IChartApi, ISeriesApi} from 'lightweight-charts';
import {Subject, takeUntil} from "rxjs";
import {DashService} from "../../service/dash.service";
import {KlineDataStruct} from "../../models/dash.model";

@Component({
  selector: 'app-trading-view',
  templateUrl: './trading-view.component.html',
  styleUrls: ['./trading-view.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TradingViewComponent implements OnInit {

  @ViewChild("chart") chart!: ElementRef;
  _ngUnsubscribe$ = new Subject<string>();
  candlestickSeries: ISeriesApi<'Candlestick'> | null = null;
  chartInstance: IChartApi | null = null;
  klineData: KlineDataStruct[] = [];

  constructor(private dashService: DashService, private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.dashService.klineData$.pipe(takeUntil(this._ngUnsubscribe$))
      .subscribe((it) => {
        this.klineData = [];
        for (const val of it) {
          if (val['open']) {
            const timestamp = new Date(val['timestamp']);
            const year = timestamp.getFullYear();
            const month = ('0' + (timestamp.getMonth() + 1)).slice(-2); // Adding 1 because January is 0
            const day = ('0' + timestamp.getDate()).slice(-2);
            const formattedDate = `${year}-${month}-${day}`;
            this.klineData.push({
                open: +val['open'],
                high: +val['high'],
                low: +val['low'],
                close: +val['low'],
                time: formattedDate
              });
          }
        }
        if (this.chartInstance) {
          this.chartInstance!.remove();
        }
        this.createChart();
        this.cdr.detectChanges();
      });

  }

  createChart() {
    const chartOptions = {
      layout: {
        textColor: 'black',
        background: {type: 'solid', color: 'white'}
      }
    };
    // @ts-ignore
    this.chartInstance = createChart(this.chart.nativeElement, chartOptions);
    this.candlestickSeries = this.chartInstance.addCandlestickSeries({
      upColor: '#26a69a',
      downColor: '#ef5350',
      borderVisible: false,
      wickUpColor: '#26a69a',
      wickDownColor: '#ef5350'
    });

    // @ts-ignore
    this.candlestickSeries.setData(this.klineData);

    this.chartInstance.timeScale().fitContent();
  }

}
