import {TokensSearchComponent} from "./tokens-search/tokens-search.component";
import {TradingViewComponent} from "./trading-view/trading-view.component";

export const components = [
  TokensSearchComponent,
  TradingViewComponent
];

export * from './trading-view/trading-view.component';
export * from './tokens-search/tokens-search.component';
