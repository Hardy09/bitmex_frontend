import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  debounceTime,
  distinctUntilChanged, filter,
  map, merge,
  Observable,
  OperatorFunction,
  Subject, Subscription,
  takeUntil
} from "rxjs";
import {DashService} from "../../service/dash.service";
import {NgbTypeahead} from "@ng-bootstrap/ng-bootstrap";
import {SocketService} from "../../service/socket.service";

@Component({
  selector: 'app-tokens-search',
  templateUrl: './tokens-search.component.html',
  styleUrls: ['./tokens-search.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TokensSearchComponent implements OnInit, OnDestroy {

  _ngUnsubscribe$ = new Subject<string>();
  selectedSymbol: any;
  allSymbols: string[] = [];
	@ViewChild('instance', { static: true }) instance!: NgbTypeahead;
  focus$ = new Subject<string>();
	click$ = new Subject<string>();
  subscription!: Subscription;

	search: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) => {
		const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
		const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
		const inputFocus$ = this.focus$;

		return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
			map((term) =>
				(term === '' ? this.allSymbols : this.allSymbols.filter((v) => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10),
			),
		);
	};


  constructor(private dashService: DashService, private cdr: ChangeDetectorRef,
              private socketService: SocketService) {
  }

  ngOnInit(): void {
    this.dashService.allCoins$.pipe(takeUntil(this._ngUnsubscribe$))
      .subscribe((it) => {
        this.allSymbols = it.map(it => it['symbol']);
        this.cdr.detectChanges();
      });
    this.dashService.getAllSymbols();
  }

  ngOnDestroy(): void {
    this._ngUnsubscribe$.next('');
    this._ngUnsubscribe$.unsubscribe();
  }

  getChartData() {
    console.log(this.selectedSymbol);
    if (this.selectedSymbol) {
      this.dashService.getKlines({symbol: this.selectedSymbol, binSize: '1d'});
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      this.socketService.openSocket(this.selectedSymbol, '1d')
    }
  }

}
