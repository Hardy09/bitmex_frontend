import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TokensSearchComponent } from './tokens-search.component';

describe('TokensSearchComponent', () => {
  let component: TokensSearchComponent;
  let fixture: ComponentFixture<TokensSearchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TokensSearchComponent]
    });
    fixture = TestBed.createComponent(TokensSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
