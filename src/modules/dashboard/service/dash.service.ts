import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {catchError, Subject, throwError} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class DashService {

  private _allCoins$ = new Subject<[]>();
  private _klineData$ = new Subject<[]>();

  constructor(private httpClient: HttpClient) { }

  getAllSymbols() {
    let mainUrl = environment.apiPrefix + 'bitmex/symbols';
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.httpClient.get(mainUrl, { headers: httpHeaders }).subscribe(
      (res : any) => {
        if (res.success) {
          try {
            this._allCoins$.next(res.result.message);
          } catch (e) {
            console.log(e);
          }
        }
      },catchError((err) => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  getKlines(filters: {symbol: string, binSize?: string}) {
    let mainUrl = environment.apiPrefix + 'bitmex/symbol/kline';
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.httpClient.get(mainUrl, { headers: httpHeaders,params: filters }).subscribe(
      (res : any) => {
        if (res.success) {
          try {
            this._klineData$.next(res.result.message);
          } catch (e) {
            console.log(e);
          }
        }
      },catchError((err) => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  get allCoins$() {
    return this._allCoins$.asObservable();
  }

  get klineData$() {
    return this._klineData$.asObservable();
  }

}
