import {webSocket} from "rxjs/webSocket";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  subject!: any;
  //static socketSubscription$ = new Subject<any>();
  static isSocketOpened = false;

  openSocket(selectedSymbol: string,time: string) {
    if (SocketService.isSocketOpened) {
      SocketService.isSocketOpened = false;
      this.disposeSocket(selectedSymbol,time);
      return; // Return if socket is already opened
    }
    this.subject = webSocket('wss://ws.bitmex.com/realtime?' +
      `subscribe=tradeBin${time}:${selectedSymbol}`);
    this.subject.subscribe(
      (it: any) => {
        console.log("SOCKET DATA ",it);
        //SocketService.socketSubscription$.next(it);
      }, (error: any) => {
        SocketService.isSocketOpened = false;
        this.disposeSocket(selectedSymbol,time);
      }
    );
    SocketService.isSocketOpened = true;
  }

  disposeSocket(selectedSymbol: string,time: string) {
    this.subject.complete();
    setTimeout(() => {
      this.openSocket(selectedSymbol,time);
    }, 8000);
  }

}
