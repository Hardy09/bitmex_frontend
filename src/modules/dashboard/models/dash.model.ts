export interface KlineDataStruct {
  open: number,
  high: number,
  low: number,
  close: number,
  time: string
}
