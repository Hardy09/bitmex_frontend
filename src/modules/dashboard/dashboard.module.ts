import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import * as DashboardContainers from './containers';
import * as DashboardComponents from './components';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgbTypeahead} from "@ng-bootstrap/ng-bootstrap";


@NgModule({
  declarations: [
    ...DashboardContainers.containers,
    ...DashboardComponents.components,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbTypeahead,
  ],
  exports: [
    ...DashboardContainers.containers,
    ...DashboardComponents.components,
  ]
})
export class DashboardModule {
}
